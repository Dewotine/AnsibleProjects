# https://en.wikipedia.org/wiki/List_of_SMTP_server_return_codes
#  py.test -v testinfra/postfix.py

import smtplib
import pytest
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import ssl

_DEFAULT_CIPHERS = (
    'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+HIGH:'
    'DH+HIGH:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES:!aNULL:'
    '!eNULL:!MD5')


@pytest.fixture
def postfix_connexion():
    smtp_connexion = smtplib.SMTP('mail.bleschet.fr', 587)
    return smtp_connexion


@pytest.fixture
def postfix_starttls_connexion(postfix_connexion):
    # only TLSv1 or higher
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    context.options |= ssl.OP_NO_SSLv2
    context.options |= ssl.OP_NO_SSLv3

    context.set_ciphers(_DEFAULT_CIPHERS)
    context.set_default_verify_paths()
    context.verify_mode = ssl.CERT_REQUIRED

    smtp_starttls_connexion = smtplib.SMTP('mail.bleschet.fr', 587)
    smtp_starttls_connexion.starttls(context=context)
    return smtp_starttls_connexion


def test_postfix_connexion(host, postfix_connexion):
    status = postfix_connexion.noop()[0]
    assert(status == 250)


def test_postfix_starttls(host, postfix_connexion):
    # only TLSv1 or higher
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    context.options |= ssl.OP_NO_SSLv2
    context.options |= ssl.OP_NO_SSLv3

    context.set_ciphers(_DEFAULT_CIPHERS)
    context.set_default_verify_paths()
    context.verify_mode = ssl.CERT_REQUIRED
    status = postfix_connexion.starttls(context=context)[0]
    assert(status == 220)
    postfix_connexion.quit()


def test_postfix_login(host, postfix_starttls_connexion):
    status = postfix_starttls_connexion.login('test@bleschet.fr', 'LLgzSnzXuu6Gw8xLUOun')[0]
    assert (status == 235)
    postfix_starttls_connexion.quit()


def test_postfix_send_message(host, postfix_starttls_connexion):
    message = MIMEMultipart()
    message['From'] = 'test@bleschet.fr'
    message['To'] = 'cedric@bleschet.fr'
    message['Subject'] = 'Courriel de test'
    body = "Corps du message de test"
    message.attach(MIMEText(body, 'html'))

    postfix_starttls_connexion.login(message['From'], 'LLgzSnzXuu6Gw8xLUOun')
    status = postfix_starttls_connexion.sendmail(message['From'], message['To'], message.as_string())