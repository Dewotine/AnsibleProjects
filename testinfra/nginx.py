# Ro lauch : py.test -v testinfra/nginx_remote.py

import pytest


@pytest.mark.parametrize("url, port", [
    ("piwigo.bleschet.fr", '80'),
    ("piwigo.bleschet.fr", '443'),
    ("webmail.bleschet.fr", '80'),
    ("webmail.bleschet.fr", '443'),
    ("blog.bleschet.fr", '80'),
    ("blog.bleschet.fr", '443'),
])
def test_nginx_vhosts(host, port, url):
    s = host.addr(url)
    assert s.port(port).is_reachable


@pytest.mark.parametrize("url, expected_code", [
    ("piwigo.bleschet.fr", '200'),
    ("webmail.bleschet.fr", '401'),
    ("blog.bleschet.fr", '200'),
])
def test_website(host, url, expected_code):
    command = host.run('curl --silent --output /dev/null --write-out %{{http_code}} "https://{0}"'.format(url))
    print(command.stdout)
    assert command.stdout.rstrip() == expected_code
    # assert command.rc == 0


@pytest.mark.parametrize("url, title", [
    ("piwigo.bleschet.fr", "Galeries de Dewotine"),
    ("webmail.bleschet.fr", "Roundcube Webmail à moi"),
    ("blog.bleschet.fr", '/en')
])
def test_website_title(host, url, title):
    command = host.run('curl --silent "https://{0}" |grep "<title>" | grep -c "{1}"'.format(url, title))
    assert command.stdout.rstrip() == '1'


@pytest.mark.parametrize('url', [
    'piwigo.bleschet.fr', 'webmail.bleschet.fr', 'blog.bleschet.fr'
])
def test_website_response_time(host, url):
    # Force LANG=en_EN.UTF-8 so the result will not use ',' as decimal separator to avoid float conversion error
    command = host.run('env LANG=en_EN.UTF-8 curl --silent "https://{0}" -o /dev/null -w %{{time_total}} '.format(url))
    response_time = float(command.stdout.rstrip())
    assert response_time < 0.3


@pytest.mark.parametrize('url', [
    'piwigo.bleschet.fr', 'webmail.bleschet.fr', 'blog.bleschet.fr'
])
def test_ssl_website(host, url):
    command = host.run('curl --silent "https://{0}" -o /dev/null -w %{{ssl_verify_result}} '.format(url))
    assert command.stdout.rstrip() == '0'
