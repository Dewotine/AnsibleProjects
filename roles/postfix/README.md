# Role: postfix

## Parameters

| Variable | Type | Description | Default |
| --- | --- | --- | --- |
| __postfix_aliases__                   	| list | Aliases to ensure present in `/etc/aliases` | [] |
| __postfix_certificate__                 	| string | Certificate used by Postfix | "default" |
| __postfix_certificate_request__   		| string | Certificate request used by Postfix | "default" |
| __postfix_conf_submission__     			| boolean | Acticvate submission | "default" |
| __postfix_disable_vrfy_command__ 			| boolean | Disable the `SMTP VRFY` command. This stops some techniques used to harvest email addresses | false |
| __postfix_domain__               			| string | Posfix Domain | "default" |
| __postfix_generic__       				| list | Generic table address mapping in `/etc/postfix/generic` | "default" |
| __postfix_hostname__               		| string | DNS Name of Postfix server | "default" |
| __postfix_inet_interfaces__           	| string | Network interfaces | "default" |
| __postfix_inet_protocols__             	| string | The Internet protocols Postfix will attempt to use when making or accepting connections | "default" |
| __postfix_mailname__               		| string | Mail name | "default" |
| __postfix_mailbox_size_limit__    		| int | Message limit size | "default" |
| __postfix_message_size_limit__        	| int | Mailbox limit size | "default" |
| __postfix_mydestination__             	| string | Specifies what domains this machine will deliver locally, instead of forwarding to another machine | "default" |
| __postfix_mynetworks__            		| list | The list of "trusted" remote SMTP clients | "default" |
| __postfix_mysql_virtual__          		| string | "default vhost" | "default" |
| __postfix_opendkim__            			| boolean | Activate OpenDKIM | "default" |
| __postfix_relayhost__            			| string | "default vhost" | "/usr/share/postfix/html" |
| __postfix_relayhost_port__ 				| int | Relay port | 587 |
| __postfix_relaytls__               		| string | Relay TLS  | "default" |
| __postfix_sasl_auth_enable__              | string | "default vhost" | "default" |
| __postfix_sasl_password__					| string | SASL relay password | "default" |
| __postfix_sasl_security_options__			| string | SMTP client SASL security options | "default" |
| __postfix_sasl_mechanism_filter__			| string | SMTP client SASL authentication mechanism filter | "default" |
| __postfix_sender_canonical_maps__    		| string | "default vhost" | "default" |
| __postfix_smtpd_banner__                	| string | Greeting banner **You MUST specify $myhostname at the start of the text. This is required by the SMTP protocol.** | "default" |
| __postfix_smtpd_tls_protocols__ 			| string | "default vhost" | "default" |
| __postfix_smtpd_helo_restrictions__		| list | "default vhost" | "default" |
| __postfix_smtpd_recipient_restrictions__	| string | "default vhost" | "default" |
| __postfix_smtpd_relay_restrictions__		| string | "default vhost" | "default" |
| __postfix_tls_enable__         			| boolean | Enable TLS | "default" |
| __postfix_use_spamassassin__  			| boolean | Activate Spamassassin | "default" |
| __postfix_virtual_aliases__  				| string | "default vhost" | "default" |
| __postfix_virtual_transport__				| string | "default vhost" | "default" |

## Playbook 
---
- hosts: "{{ filter }}:&dc_{{ dc }}:&env_{{ env }}"
  become: true
  roles:
    - postfix

## Usage
### Deploy Postfix SMTP Relay
```
ansible-playbook -v playbooks/configuration_mgmt/mail/postfix.yml -e filter="" -e dc=corp -e env=corp_datacenter
```
### Deploy Postfix Client on a specific host
```
ansible-playbook -v playbooks/configuration_mgmt/mail/postfix.yml -e filter="monitoring-es-data02.corp.ipd-auto.cloud" -e dc=corp -e env=corp_datacenter
```  