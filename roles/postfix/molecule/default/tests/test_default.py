import pytest

@pytest.mark.parametrize('package', [
    'postfix',
    'postfix-mysql'
])
def test_postfix_is_installed(host, package):
    """
    Ensures that the postfix and its mariadb depencency is installed in version 3.4.x
    """
    postfix = host.package(package)
    assert postfix.is_installed
    assert postfix.version.startswith("3.5")

@pytest.mark.parametrize('package', [
    'postfix-sqlite',
])
def test_postfix_useless_dependency(host, package):
    """
    Ensures that the postfix has not installed useless dependency
    """
    postfix = host.package(package)
    assert not postfix.is_installed


def test_postfix_running_and_enabled(host):
    """
    Ensures that the postfix service is enabled and is running
    """
    postfix = host.service("postfix")
    assert postfix.is_running
    assert postfix.is_enabled

def test_postfix_config(host):
    postfix_conf_status = host.run("postconf -n")
    assert postfix_conf_status.exit_status == 0

@pytest.mark.parametrize('port', [
        '25','587'
    ])
def test_postfix_socket(host,port):
    s = host.socket('tcp://0.0.0.0:%s' % port)
    assert s.is_listening

# todo: check if '/etc/aliases.db' is present
