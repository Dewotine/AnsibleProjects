# py.test -v --hosts=cbleschet@vps528223.ovh.net roles/dovecot/molecule/default/tests/test_default.py --sudo

import warnings
import pytest


@pytest.mark.parametrize('package', [
    'dovecot-core',
    'dovecot-imapd',
    'dovecot-lmtpd',
    'dovecot-mysql'
])
def test_dovecot_is_installed(host, package):
    dovecot = host.package(package)
    assert dovecot.is_installed
    assert dovecot.version.startswith("1:2.3")


@pytest.mark.parametrize('package', [
    'dovecot-pop3d',
])
def test_dovecot_useless_packages(host, package):
    """
    Check that dovecot did not install useless dependencies (pop3)
    """
    dovecot = host.package(package)
    if dovecot.is_installed:
        warnings.warn(package + " should not be installed")


def test_dovecot_running_and_enabled(host):
    """
    Ensures that the dovecot service is enabled and is running
    """
    dovecot = host.service("dovecot")
    assert dovecot.is_running
    assert dovecot.is_enabled


def test_dovecot_ssl_min_protocol(host):
    """
    Test the minimal SSL version supported
    """
    dovecot_ssl_min_protocol = host.run("dovecot -n | grep ssl_min_protocol | cut -d'=' -f2 | tr -d ' ' | tr -d '\n'")
    print("dovecot_ssl_min_protocol : {0}".format(dovecot_ssl_min_protocol))
    assert dovecot_ssl_min_protocol.exit_status == 0
    assert dovecot_ssl_min_protocol.stdout == 'TLSv1.2'


@pytest.mark.parametrize('port', [
    '993',
    '443'
])
def test_dovecot_socket(host, port):
    """
    Verify dovecot port
    """
    s = host.socket('tcp://0.0.0.0:%s' % port)
    assert s.is_listening

# def test_check_dovecot_logs(host):
#     daemon_file_content = host.file('/etc/docker/daemon.json').content
#     assert '{"base":"172.48.0.0/16", "size":16}' in daemon_file_content


#
# def test_nginx_process(Process):
#     master = Process.get(pid=1)
#     assert master.args == "nginx: master process nginx"
#     workers = Process.filter(ppid=master.pid)
#     assert len(workers) == 4
#
#
