import pytest

@pytest.mark.parametrize('package', [
    'mariadb-common',
    'mariadb-client-10.4',
    'mariadb-server-core-10.4',
    'mariadb-server-10.4'
])
def test_mariadb_is_installed(host, package):
    """
    Ensures that the mariadb and its mariadb depencency is installed in version 3.4.x
    """
    mariadb = host.package(package)
    assert mariadb.is_installed
    assert mariadb.version.startswith("1:10.4")

# @pytest.mark.parametrize('package', [
#     'mariadb-sqlite',
# ])
# def test_mariadb_useless_dependency(host, package):
#     """
#     Ensures that the mariadb has not installed useless dependency
#     """
#     mariadb = host.package(package)
#     assert not mariadb.is_installed
#
#
def test_mariadb_running_and_enabled(host):
    """
    Ensures that the mariadb service is enabled and is running
    """
    mariadb = host.service("mariadb")
    assert mariadb.is_running
    assert mariadb.is_enabled
#
# def test_mariadb_config(host):
#     mariadb_conf_status = host.run("postconf -n")
#     assert mariadb_conf_status.exit_status == 0
#
@pytest.mark.parametrize('port', [
        '3306'
    ])
def test_mariadb_socket(host,port):
    s = host.socket('tcp://0.0.0.0:%s' % port)
    assert s.is_listening
