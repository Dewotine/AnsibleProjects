---
# Doc MariaDB: https://mariadb.org/download/?t=repo-config&d=Debian+11+%22Bullseye%22&v=10.11&r_m=icam
- name: "Create {{ mariadb_keyring_folder }} folder"
  file:
    path: "{{ mariadb_keyring_folder }}"
    state: directory

- name: "Download key from mariadb site"
  get_url:
    url: "{{ mariadb_signing_key_url }}"
    dest: "{{ mariadb_keyring_folder }}/mariadb-keyring.pgp"

- name: "Configure MariaDB repo"
  template:
    src: "mariadb.sources.j2"
    dest: "/etc/apt/sources.list.d/mariadb.sources"

- name: "Check if mariadb has already been installed (enabled)"
  command: systemctl is-enabled mariadb.service
  register: mariadb_installed
  failed_when: mariadb_installed.rc not in [0,1]
  changed_when: "'enabled' not in mariadb_installed.stdout_lines"

- debug:
    msg: 'MariaDB current state => {{ mariadb_installed.stdout_lines }}'

- name: "Add MariaDB repository preferences"
  template:
    src: mariadb.j2
    dest: /etc/apt/preferences.d/mariadb
  when: mariadb_apt_pin_priority is defined

- name: "Update APT cache"
  apt:
    update_cache: yes
  register: __update_cache
  retries: 3
  delay: 10
  until: __update_cache is not failed

- name: "Install python prerequisite (for ansible mysql_user module)"
  apt:
    name:
      - "python3-pymysql"
    state: present

- name: "Ensure MariaDB packages are installed"
  apt:
    name:
      - "mariadb-client"
      - "mariadb-server"
    state: present
  register: mariadb_install_packages

# Because Debian starts MySQL as part of the install process, we need to stop
# mysql and remove the logfiles in case the user set a custom log file size.
- name: "Ensure MySQL is stopped after initial install"
  systemd:
    name: "{{ mariadb_service }}"
    state: stopped
  when: not mariadb_installed.stdout_lines or mariadb_installed.stdout_lines == 'disabled'

- name: "Delete initial datadir (/var/lib/mysql)"
  no_log: true
  file:
    path: "{{ mariadb_initial_datadir }}"
    state: absent
  when: not mariadb_installed.stdout_lines or mariadb_installed.stdout_lines == 'disabled'

- name: "Create link between /var/lib/mysql and our datadir"
  file:
    src: "{{ mariadb_datadir }}"
    dest: "{{ mariadb_initial_datadir }}"
    state: link
    force: yes
  when: mariadb_datadir != mariadb_initial_datadir

- name: "Delete mysql log folder (use mariadb instead)"
  file:
    path: "/var/log/mysql"
    state: absent