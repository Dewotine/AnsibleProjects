---
- name: "Ensure replication user exists on master (from master)"
  mysql_user:
    login_unix_socket: "{{ mariadb_socket }}"
    name: "{{ mariadb_replication_user.name }}"
    host: "{{ mariadb_replication_user.host | default('%') }}"
    password: "{{ mariadb_replication_user.password }}"
    priv: "*.*:REPLICATION SLAVE,REPLICATION CLIENT"
    state: present
  when:
    - mariadb_replication_role == 'master'
    - mariadb_replication_master != ''

- name: "Ensure replication user exists on master (from slave)"
  mysql_user:
    login_unix_socket: "{{ mariadb_socket }}"
    name: "{{ mariadb_replication_user.name }}"
    host: "{{ mariadb_replication_user.host | default('%') }}"
    password: "{{ mariadb_replication_user.password }}"
    priv: "*.*:REPLICATION SLAVE,REPLICATION CLIENT"
    state: present
  delegate_to: '{{ mariadb_replication_master |regex_replace("."+dc_domain_name) }}'
  when:
    - mariadb_replication_role == 'slave'
    - mariadb_replication_master != ''

- name: "Check slave replication status"
  mysql_replication:
    login_unix_socket: "{{ mariadb_socket }}"
    mode: getslave
  ignore_errors: yes
  register: slave
  when:
    - mariadb_replication_role == 'slave'
    - mariadb_replication_master != ''

- name: "Check master replication status"
  mysql_replication:
    mode: getmaster
    login_host: "{{ mariadb_replication_master }}"
    login_user: "{{ mariadb_replication_user.name }}"
    login_password: "{{ mariadb_replication_user.password }}"
    login_port: "{{ mariadb_port }}"
  register: master
  ignore_errors: True
  when:
    - mariadb_replication_role == 'slave'
    - not slave.Is_Slave
    - mariadb_replication_master != ''

- name: "Configure replication on the slave."
  mysql_replication:
    login_unix_socket: "{{ mariadb_socket }}"
    mode: changemaster
    master_host: "{{ mariadb_replication_master }}"
    master_user: "{{ mariadb_replication_user.name }}"
    master_password: "{{ mariadb_replication_user.password }}"
    master_log_file: "{{ master.File }}"
    master_log_pos: "{{ master.Position }}"
    master_port: "{{ mariadb_port }}"
  ignore_errors: True
  when:
    - mariadb_replication_role == 'slave'
    - not slave.Is_Slave
    - mariadb_replication_master != ''

- name: "Start replication"
  mysql_replication:
    login_unix_socket: "{{ mariadb_socket }}"
    mode: startslave
  when:
    - mariadb_replication_role == 'slave'
    - not slave.Is_Slave
    - mariadb_replication_master != ''
