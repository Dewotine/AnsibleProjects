import pytest
import requests


def test_nginx_is_installed(host):
    nginx = host.package("nginx")
    assert nginx.is_installed
    assert nginx.version.startswith("1.18")


def test_nginx_running_and_enabled(host):
    """
    Ensures that the nginx service is enabled and is running
    """
    nginx = host.service("nginx")
    assert nginx.is_running
    assert nginx.is_enabled


def test_nginx_config(host):
    nginx_conf_status = host.run("nginx -t")
    assert nginx_conf_status.exit_status == 0

@pytest.mark.parametrize('port', [
        '80','443'
    ])
def test_nginx_socket(host,port):
    s = host.socket('tcp://0.0.0.0:%s' % port)
    assert s.is_listening

#
# def test_nginx_process(Process):
#     master = Process.get(pid=1)
#     assert master.args == "nginx: master process nginx"
#     workers = Process.filter(ppid=master.pid)
#     assert len(workers) == 4
#
#


