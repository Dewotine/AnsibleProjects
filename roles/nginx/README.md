# Role: nginx

## Parameters

| Variable | Type | Description | Default |
| --- | --- | --- | --- |
| __nginx_aliases__                           | list | alias | []] ||
| __nginx_apt_key_url__                           | string | "default vhost" | "default" |
| __nginx_configuration_folder__                           | string | "default vhost" | "default" |
| __nginx_domain__                           | string | "default vhost" | "default" |
| __nginx_headers_list__                           | string | "default vhost" | "default" |
| __nginx_use_nginx_status__                 |boolean|true| true|
| __nginx_repo_url__                           | string | "default vhost" | "default" |
| __nginx_server_name__                           | string | "default vhost" | "default" |
| __nginx_ssl_ciphers__                           | string | "default vhost" | "default" |
| __nginx_ssl_port__                           | string | "default vhost" | "default" |
| __nginx_ssl_protocols_                           | string | "default vhost" | "default" |
| __nginx_ssl_session_cache__                           | string | "default vhost" | "default" |
| __nginx_ssl_timeout__                           | string | "default vhost" | "default" |
| __nginx_site__                           | string | "default vhost" | "default" |
| __nginx_nginx_sites_to_deploy__                           | string | "default vhost" | "default" |
| __nginx_use_letsencrypt__                           | string | "default vhost" | "default" |
| __nginx_webroot__                           | string | "default vhost" | "/usr/share/nginx/html" |
-