-- MariaDB dump 10.19  Distrib 10.5.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: piwigo
-- ------------------------------------------------------
-- Server version	10.5.12-MariaDB-1:10.5.12+maria~bullseye-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `piwigo_activity`
--

DROP TABLE IF EXISTS `piwigo_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_activity` (
                                   `activity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                   `object` varchar(255) NOT NULL,
                                   `object_id` int(11) unsigned NOT NULL,
                                   `action` varchar(255) NOT NULL,
                                   `performed_by` mediumint(8) unsigned NOT NULL,
                                   `session_idx` varchar(255) NOT NULL,
                                   `ip_address` varchar(50) DEFAULT NULL,
                                   `occured_on` timestamp NOT NULL DEFAULT current_timestamp(),
                                   `details` varchar(255) DEFAULT NULL,
                                   PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_activity`
--

LOCK TABLES `piwigo_activity` WRITE;
/*!40000 ALTER TABLE `piwigo_activity` DISABLE KEYS */;
INSERT INTO `piwigo_activity` VALUES (1,'user',1,'login',1,'m7cov7o59as460927hqgfkffk7','192.168.1.11','2019-12-15 17:01:35','a:2:{s:6:\"script\";s:7:\"install\";s:5:\"agent\";s:135:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36\";}'),(2,'user',1,'login',1,'s38s54e7969l2k625a073q63jj','192.168.1.16','2021-02-06 15:35:59','a:2:{s:6:\"script\";s:14:\"identification\";s:5:\"agent\";s:76:\"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0\";}');
/*!40000 ALTER TABLE `piwigo_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_caddie`
--

DROP TABLE IF EXISTS `piwigo_caddie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_caddie` (
                                 `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                 `element_id` mediumint(8) NOT NULL DEFAULT 0,
                                 PRIMARY KEY (`user_id`,`element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_caddie`
--

LOCK TABLES `piwigo_caddie` WRITE;
/*!40000 ALTER TABLE `piwigo_caddie` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_caddie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_categories`
--

DROP TABLE IF EXISTS `piwigo_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_categories` (
                                     `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
                                     `name` varchar(255) NOT NULL DEFAULT '',
                                     `id_uppercat` smallint(5) unsigned DEFAULT NULL,
                                     `comment` text DEFAULT NULL,
                                     `dir` varchar(255) DEFAULT NULL,
                                     `rank` smallint(5) unsigned DEFAULT NULL,
                                     `status` enum('public','private') NOT NULL DEFAULT 'public',
                                     `site_id` tinyint(4) unsigned DEFAULT NULL,
                                     `visible` enum('true','false') NOT NULL DEFAULT 'true',
                                     `representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
                                     `uppercats` varchar(255) NOT NULL DEFAULT '',
                                     `commentable` enum('true','false') NOT NULL DEFAULT 'true',
                                     `global_rank` varchar(255) DEFAULT NULL,
                                     `image_order` varchar(128) DEFAULT NULL,
                                     `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
                                     `lastmodified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `categories_i3` (`permalink`),
                                     KEY `categories_i2` (`id_uppercat`),
                                     KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_categories`
--

LOCK TABLES `piwigo_categories` WRITE;
/*!40000 ALTER TABLE `piwigo_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_comments`
--

DROP TABLE IF EXISTS `piwigo_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_comments` (
                                   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                   `image_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                   `date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                   `author` varchar(255) DEFAULT NULL,
                                   `email` varchar(255) DEFAULT NULL,
                                   `author_id` mediumint(8) unsigned DEFAULT NULL,
                                   `anonymous_id` varchar(45) NOT NULL,
                                   `website_url` varchar(255) DEFAULT NULL,
                                   `content` longtext DEFAULT NULL,
                                   `validated` enum('true','false') NOT NULL DEFAULT 'false',
                                   `validation_date` datetime DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   KEY `comments_i2` (`validation_date`),
                                   KEY `comments_i1` (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_comments`
--

LOCK TABLES `piwigo_comments` WRITE;
/*!40000 ALTER TABLE `piwigo_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_config`
--

DROP TABLE IF EXISTS `piwigo_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_config` (
                                 `param` varchar(40) NOT NULL DEFAULT '',
                                 `value` text DEFAULT NULL,
                                 `comment` varchar(255) DEFAULT NULL,
                                 PRIMARY KEY (`param`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='configuration table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_config`
--

LOCK TABLES `piwigo_config` WRITE;
/*!40000 ALTER TABLE `piwigo_config` DISABLE KEYS */;
INSERT INTO `piwigo_config` VALUES ('activate_comments','false','Global parameter for usage of comments system'),('allow_user_customization','true','allow users to customize their gallery?'),('allow_user_registration','true','allow visitors to register?'),('blk_menubar','','Menubar options'),('c13y_ignore',NULL,'List of ignored anomalies'),('comments_author_mandatory','false','Comment author is mandatory'),('comments_email_mandatory','false','Comment email is mandatory'),('comments_enable_website','true','Enable \"website\" field on add comment form'),('comments_forall','false','even guest not registered can post comments'),('comments_order','ASC','comments order on picture page and cie'),('comments_validation','false','administrators validate users comments before becoming visible'),('data_dir_checked','1',NULL),('derivatives','a:4:{s:1:\"d\";a:9:{s:6:\"square\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:120;i:1;i:120;}s:8:\"max_crop\";i:1;s:8:\"min_size\";a:2:{i:0;i:120;i:1;i:120;}}s:7:\"sharpen\";i:0;}s:5:\"thumb\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:144;i:1;i:144;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"2small\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:240;i:1;i:240;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"xsmall\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:432;i:1;i:324;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:5:\"small\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:576;i:1;i:432;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"medium\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:792;i:1;i:594;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:5:\"large\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1008;i:1;i:756;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:6:\"xlarge\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1224;i:1;i:918;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}s:7:\"xxlarge\";O:16:\"DerivativeParams\":3:{s:13:\"last_mod_time\";i:1576429299;s:6:\"sizing\";O:12:\"SizingParams\":3:{s:10:\"ideal_size\";a:2:{i:0;i:1656;i:1;i:1242;}s:8:\"max_crop\";i:0;s:8:\"min_size\";N;}s:7:\"sharpen\";i:0;}}s:1:\"q\";i:95;s:1:\"w\";O:15:\"WatermarkParams\":7:{s:4:\"file\";s:0:\"\";s:8:\"min_size\";a:2:{i:0;i:500;i:1;i:500;}s:4:\"xpos\";i:50;s:4:\"ypos\";i:50;s:7:\"xrepeat\";i:0;s:7:\"yrepeat\";i:0;s:7:\"opacity\";i:100;}s:1:\"c\";a:0:{}}',NULL),('display_fromto','false',NULL),('email_admin_on_comment','false','Send an email to the administrators when a valid comment is entered'),('email_admin_on_comment_deletion','false','Send an email to the administrators when a comment is deleted'),('email_admin_on_comment_edition','false','Send an email to the administrators when a comment is modified'),('email_admin_on_comment_validation','true','Send an email to the administrators when a comment requires validation'),('email_admin_on_new_user','false','Send an email to theadministrators when a user registers'),('extents_for_templates','a:0:{}','Actived template-extension(s)'),('gallery_locked','false','Lock your gallery temporary for non admin users'),('gallery_title','Une galerie Piwigo de plus','Title at top of each page and for RSS feed'),('history_admin','false','keep a history of administrator visits on your website'),('history_guest','true','keep a history of guest visits on your website'),('history_sections_cache','a:9:{i:0;s:10:\"categories\";i:1;s:4:\"tags\";i:2;s:6:\"search\";i:3;s:4:\"list\";i:4;s:9:\"favorites\";i:5;s:12:\"most_visited\";i:6;s:10:\"best_rated\";i:7;s:11:\"recent_pics\";i:8;s:11:\"recent_cats\";}',NULL),('index_caddie_icon','true',NULL),('index_created_date_icon','true','Display calendar by creation date icon'),('index_edit_icon','true',NULL),('index_flat_icon','false','Display flat icon'),('index_new_icon','true','Display new icons next albums and pictures'),('index_posted_date_icon','true','Display calendar by posted date'),('index_sizes_icon','true',NULL),('index_slideshow_icon','true','Display slideshow icon'),('index_sort_order_input','true','Display image order selection list'),('log','true','keep an history of visits on your website'),('mail_theme','clear',NULL),('menubar_filter_icon','false','Display filter icon'),('mobile_theme','smartpocket',NULL),('modus_theme','a:5:{s:4:\"skin\";s:9:\"newspaper\";s:16:\"album_thumb_size\";i:250;s:17:\"index_photo_deriv\";s:6:\"2small\";s:22:\"index_photo_deriv_hdpi\";s:6:\"xsmall\";s:19:\"display_page_banner\";b:0;}',NULL),('nbm_complementary_mail_content','','Complementary mail content for notification by mail'),('nbm_send_detailed_content','true','Send detailed content for notification by mail'),('nbm_send_html_mail','true','Send mail on HTML format for notification by mail'),('nbm_send_mail_as','','Send mail as param value for notification by mail'),('nbm_send_recent_post_dates','true','Send recent post by dates for notification by mail'),('nb_categories_page','12','Param for categories pagination'),('nb_comment_page','10','number of comments to display on each page'),('no_photo_yet','false',NULL),('obligatory_user_mail_address','false','Mail address is obligatory for users'),('order_by','ORDER BY date_available DESC, file ASC, id ASC','default photo order'),('order_by_inside_category','ORDER BY date_available DESC, file ASC, id ASC','default photo order inside category'),('original_resize','false',NULL),('original_resize_maxheight','2016',NULL),('original_resize_maxwidth','2016',NULL),('original_resize_quality','95',NULL),('page_banner','<h1>%gallery_title%</h1>\n\n<p>Bienvenue sur ma galerie photo</p>','html displayed on the top each page of your gallery'),('picture_caddie_icon','true',NULL),('picture_download_icon','true','Display download icon on picture page'),('picture_edit_icon','true',NULL),('picture_favorite_icon','true','Display favorite icon on picture page'),('picture_informations','a:11:{s:6:\"author\";b:1;s:10:\"created_on\";b:1;s:9:\"posted_on\";b:1;s:10:\"dimensions\";b:0;s:4:\"file\";b:0;s:8:\"filesize\";b:0;s:4:\"tags\";b:1;s:10:\"categories\";b:1;s:6:\"visits\";b:1;s:12:\"rating_score\";b:1;s:13:\"privacy_level\";b:1;}','Information displayed on picture page'),('picture_menu','false','Show menubar on picture page'),('picture_metadata_icon','true','Display metadata icon on picture page'),('picture_navigation_icons','true','Display navigation icons on picture page'),('picture_navigation_thumb','true','Display navigation thumbnails on picture page'),('picture_representative_icon','true',NULL),('picture_sizes_icon','true',NULL),('picture_slideshow_icon','true','Display slideshow icon on picture page'),('piwigo_db_version','12',NULL),('rate','false','Rating pictures feature is enabled'),('rate_anonymous','true','Rating pictures feature is also enabled for visitors'),('secret_key','357027363ed8580b5680e19581ec101f','a secret key specific to the gallery for internal use'),('show_mobile_app_banner_in_admin','true',NULL),('show_mobile_app_banner_in_gallery','false',NULL),('smartpocket','a:2:{s:4:\"loop\";b:1;s:8:\"autohide\";i:5000;}',NULL),('updates_ignored','a:3:{s:7:\"plugins\";a:0:{}s:6:\"themes\";a:0:{}s:9:\"languages\";a:0:{}}','Extensions ignored for update'),('update_notify_last_check','2021-12-25T14:01:10+01:00',NULL),('user_can_delete_comment','false','administrators can allow user delete their own comments'),('user_can_edit_comment','false','administrators can allow user edit their own comments'),('week_starts_on','monday','Monday may not be the first day of the week');
/*!40000 ALTER TABLE `piwigo_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_favorites`
--

DROP TABLE IF EXISTS `piwigo_favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_favorites` (
                                    `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                    `image_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                    PRIMARY KEY (`user_id`,`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_favorites`
--

LOCK TABLES `piwigo_favorites` WRITE;
/*!40000 ALTER TABLE `piwigo_favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_group_access`
--

DROP TABLE IF EXISTS `piwigo_group_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_group_access` (
                                       `group_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                       `cat_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                       PRIMARY KEY (`group_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_group_access`
--

LOCK TABLES `piwigo_group_access` WRITE;
/*!40000 ALTER TABLE `piwigo_group_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_group_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_groups`
--

DROP TABLE IF EXISTS `piwigo_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_groups` (
                                 `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
                                 `name` varchar(255) NOT NULL DEFAULT '',
                                 `is_default` enum('true','false') NOT NULL DEFAULT 'false',
                                 `lastmodified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `groups_ui1` (`name`),
                                 KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_groups`
--

LOCK TABLES `piwigo_groups` WRITE;
/*!40000 ALTER TABLE `piwigo_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_history`
--

DROP TABLE IF EXISTS `piwigo_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_history` (
                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `date` date NOT NULL DEFAULT '1970-01-01',
                                  `time` time NOT NULL DEFAULT '00:00:00',
                                  `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                  `IP` varchar(15) NOT NULL DEFAULT '',
                                  `section` enum('categories','tags','search','list','favorites','most_visited','best_rated','recent_pics','recent_cats') DEFAULT NULL,
                                  `category_id` smallint(5) DEFAULT NULL,
                                  `tag_ids` varchar(50) DEFAULT NULL,
                                  `image_id` mediumint(8) DEFAULT NULL,
                                  `image_type` enum('picture','high','other') DEFAULT NULL,
                                  `format_id` int(11) unsigned DEFAULT NULL,
                                  `auth_key_id` int(11) unsigned DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_history`
--

LOCK TABLES `piwigo_history` WRITE;
/*!40000 ALTER TABLE `piwigo_history` DISABLE KEYS */;
INSERT INTO `piwigo_history` VALUES (1,'2021-12-25','14:01:10',2,'127.0.0.1','categories',NULL,NULL,NULL,NULL,NULL,NULL),(2,'2021-12-25','14:01:12',2,'127.0.0.1','categories',NULL,NULL,NULL,NULL,NULL,NULL),(3,'2021-12-25','14:01:14',2,'127.0.0.1','categories',NULL,NULL,NULL,NULL,NULL,NULL),(4,'2021-12-25','14:01:16',2,'127.0.0.1','most_visited',NULL,NULL,NULL,NULL,NULL,NULL),(5,'2021-12-25','14:01:17',2,'127.0.0.1','recent_pics',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `piwigo_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_history_summary`
--

DROP TABLE IF EXISTS `piwigo_history_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_history_summary` (
                                          `year` smallint(4) NOT NULL DEFAULT 0,
                                          `month` tinyint(2) DEFAULT NULL,
                                          `day` tinyint(2) DEFAULT NULL,
                                          `hour` tinyint(2) DEFAULT NULL,
                                          `nb_pages` int(11) DEFAULT NULL,
                                          `history_id_from` int(10) unsigned DEFAULT NULL,
                                          `history_id_to` int(10) unsigned DEFAULT NULL,
                                          UNIQUE KEY `history_summary_ymdh` (`year`,`month`,`day`,`hour`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_history_summary`
--

LOCK TABLES `piwigo_history_summary` WRITE;
/*!40000 ALTER TABLE `piwigo_history_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_history_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_image_category`
--

DROP TABLE IF EXISTS `piwigo_image_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_image_category` (
                                         `image_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                         `category_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                         `rank` mediumint(8) unsigned DEFAULT NULL,
                                         PRIMARY KEY (`image_id`,`category_id`),
                                         KEY `image_category_i1` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_image_category`
--

LOCK TABLES `piwigo_image_category` WRITE;
/*!40000 ALTER TABLE `piwigo_image_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_image_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_image_format`
--

DROP TABLE IF EXISTS `piwigo_image_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_image_format` (
                                       `format_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                       `image_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                       `ext` varchar(255) NOT NULL,
                                       `filesize` mediumint(9) unsigned DEFAULT NULL,
                                       PRIMARY KEY (`format_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_image_format`
--

LOCK TABLES `piwigo_image_format` WRITE;
/*!40000 ALTER TABLE `piwigo_image_format` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_image_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_image_tag`
--

DROP TABLE IF EXISTS `piwigo_image_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_image_tag` (
                                    `image_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                    `tag_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                    PRIMARY KEY (`image_id`,`tag_id`),
                                    KEY `image_tag_i1` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_image_tag`
--

LOCK TABLES `piwigo_image_tag` WRITE;
/*!40000 ALTER TABLE `piwigo_image_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_image_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_images`
--

DROP TABLE IF EXISTS `piwigo_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_images` (
                                 `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                                 `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                 `date_available` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                 `date_creation` datetime DEFAULT NULL,
                                 `name` varchar(255) DEFAULT NULL,
                                 `comment` text DEFAULT NULL,
                                 `author` varchar(255) DEFAULT NULL,
                                 `hit` int(10) unsigned NOT NULL DEFAULT 0,
                                 `filesize` mediumint(9) unsigned DEFAULT NULL,
                                 `width` smallint(9) unsigned DEFAULT NULL,
                                 `height` smallint(9) unsigned DEFAULT NULL,
                                 `coi` char(4) DEFAULT NULL COMMENT 'center of interest',
                                 `representative_ext` varchar(4) DEFAULT NULL,
                                 `date_metadata_update` date DEFAULT NULL,
                                 `rating_score` float(5,2) unsigned DEFAULT NULL,
                                 `path` varchar(255) NOT NULL DEFAULT '',
                                 `storage_category_id` smallint(5) unsigned DEFAULT NULL,
                                 `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
                                 `md5sum` char(32) DEFAULT NULL,
                                 `added_by` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                 `rotation` tinyint(3) unsigned DEFAULT NULL,
                                 `latitude` double(8,6) DEFAULT NULL,
                                 `longitude` double(9,6) DEFAULT NULL,
                                 `lastmodified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                                 PRIMARY KEY (`id`),
                                 KEY `images_i2` (`date_available`),
                                 KEY `images_i3` (`rating_score`),
                                 KEY `images_i4` (`hit`),
                                 KEY `images_i5` (`date_creation`),
                                 KEY `images_i1` (`storage_category_id`),
                                 KEY `images_i6` (`latitude`),
                                 KEY `lastmodified` (`lastmodified`),
                                 KEY `images_i7` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_images`
--

LOCK TABLES `piwigo_images` WRITE;
/*!40000 ALTER TABLE `piwigo_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_languages`
--

DROP TABLE IF EXISTS `piwigo_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_languages` (
                                    `id` varchar(64) NOT NULL DEFAULT '',
                                    `version` varchar(64) NOT NULL DEFAULT '0',
                                    `name` varchar(64) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_languages`
--

LOCK TABLES `piwigo_languages` WRITE;
/*!40000 ALTER TABLE `piwigo_languages` DISABLE KEYS */;
INSERT INTO `piwigo_languages` VALUES ('af_ZA','2.10.1','Afrikaans [ZA]'),('ar_EG','2.10.1','العربية (مصر) [EG]'),('ar_MA','2.10.1','العربية [MA]'),('ar_SA','2.10.1','العربية [AR]'),('az_AZ','2.10.1','Azərbaycanca [AZ]'),('bg_BG','2.10.1','Български [BG]'),('bn_IN','2.10.1','বাংলা[IN]'),('br_FR','2.10.1','Brezhoneg [FR]'),('ca_ES','2.10.1','Català [CA]'),('cs_CZ','2.10.1','Česky [CZ]'),('da_DK','2.10.1','Dansk [DK]'),('de_DE','2.10.1','Deutsch [DE]'),('dv_MV','2.10.1','Dhivehi [MV]'),('el_GR','2.10.1','Ελληνικά [GR]'),('en_GB','2.10.1','English [GB]'),('en_UK','2.10.1','English [UK]'),('en_US','2.10.1','English [US]'),('eo_EO','2.10.1','Esperanto [EO]'),('es_AR','2.10.1','Argentina [AR]'),('es_ES','2.10.1','Español [ES]'),('es_MX','2.10.1','México [MX]'),('et_EE','2.10.1','Estonian [EE]'),('eu_ES','2.10.1','Euskara [ES]'),('fa_IR','2.10.1','پارسی [IR]'),('fi_FI','2.10.1','Finnish [FI]'),('fr_CA','2.10.1','Français [QC]'),('fr_FR','2.10.1','Français [FR]'),('ga_IE','2.10.1','Gaeilge [IE]'),('gl_ES','2.10.1','Galego [ES]'),('gu_IN','2.10.1','ગુજરાતી[IN]'),('he_IL','2.10.1','עברית [IL]'),('hr_HR','2.10.1','Hrvatski [HR]'),('hu_HU','2.10.1','Magyar [HU]'),('id_ID','2.10.1','Bahasa Indonesia [ID]'),('is_IS','2.10.1','Íslenska [IS]'),('it_IT','2.10.1','Italiano [IT]'),('ja_JP','2.10.1','日本語 [JP]'),('ka_GE','2.10.1','ქართული [GE]'),('km_KH','2.10.1','ខ្មែរ [KH]'),('kn_IN','2.10.1','ಕನ್ನಡ [IN]'),('kok_IN','2.10.1','कोंकणी [IN]'),('ko_KR','2.10.1','한국어 [KR]'),('lb_LU','2.10.1','Lëtzebuergesch [LU]'),('lt_LT','2.10.1','Lietuviu [LT]'),('lv_LV','2.10.1','Latviešu [LV]'),('mk_MK','2.10.1','Македонски [MK]'),('mn_MN','2.10.1','Монгол [MN]'),('ms_MY','2.10.1','Malay [MY]'),('nb_NO','2.10.1','Norsk bokmål [NO]'),('nl_NL','2.10.1','Nederlands [NL]'),('nn_NO','2.10.1','Norwegian nynorsk [NO]'),('pl_PL','2.10.1','Polski [PL]'),('pt_BR','2.10.1','Brasil [BR]'),('pt_PT','2.10.1','Português [PT]'),('ro_RO','2.10.1','Română [RO]'),('ru_RU','2.10.1','Русский [RU]'),('sh_RS','2.10.1','Srpski [SR]'),('sk_SK','2.10.1','Slovensky [SK]'),('sl_SI','2.10.1','Slovenšcina [SI]'),('sr_RS','2.10.1','Српски [SR]'),('sv_SE','2.10.1','Svenska [SE]'),('ta_IN','2.10.1','தமிழ் [IN]'),('th_TH','2.10.1','ภาษาไทย [TH]'),('tr_TR','2.10.1','Türkçe [TR]'),('uk_UA','2.10.1','Українська [UA]'),('vi_VN','2.10.1','Tiếng Việt [VN]'),('wo_SN','2.10.1','Wolof [SN]'),('zh_CN','2.10.1','简体中文 [CN]'),('zh_HK','2.10.1','中文 (香港) [HK]'),('zh_TW','2.10.1','中文 (繁體) [TW]');
/*!40000 ALTER TABLE `piwigo_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_lounge`
--

DROP TABLE IF EXISTS `piwigo_lounge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_lounge` (
                                 `image_id` mediumint(8) unsigned NOT NULL,
                                 `category_id` smallint(5) unsigned NOT NULL,
                                 PRIMARY KEY (`image_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_lounge`
--

LOCK TABLES `piwigo_lounge` WRITE;
/*!40000 ALTER TABLE `piwigo_lounge` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_lounge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_old_permalinks`
--

DROP TABLE IF EXISTS `piwigo_old_permalinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_old_permalinks` (
                                         `cat_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                         `permalink` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                         `date_deleted` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                         `last_hit` datetime DEFAULT NULL,
                                         `hit` int(10) unsigned NOT NULL DEFAULT 0,
                                         PRIMARY KEY (`permalink`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_old_permalinks`
--

LOCK TABLES `piwigo_old_permalinks` WRITE;
/*!40000 ALTER TABLE `piwigo_old_permalinks` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_old_permalinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_plugins`
--

DROP TABLE IF EXISTS `piwigo_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_plugins` (
                                  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                  `state` enum('inactive','active') NOT NULL DEFAULT 'inactive',
                                  `version` varchar(64) NOT NULL DEFAULT '0',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_plugins`
--

LOCK TABLES `piwigo_plugins` WRITE;
/*!40000 ALTER TABLE `piwigo_plugins` DISABLE KEYS */;
INSERT INTO `piwigo_plugins` VALUES ('TakeATour','active','12.1.0');
/*!40000 ALTER TABLE `piwigo_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_rate`
--

DROP TABLE IF EXISTS `piwigo_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_rate` (
                               `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                               `element_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                               `anonymous_id` varchar(45) NOT NULL DEFAULT '',
                               `rate` tinyint(2) unsigned NOT NULL DEFAULT 0,
                               `date` date NOT NULL DEFAULT '1970-01-01',
                               PRIMARY KEY (`element_id`,`user_id`,`anonymous_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_rate`
--

LOCK TABLES `piwigo_rate` WRITE;
/*!40000 ALTER TABLE `piwigo_rate` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_search`
--

DROP TABLE IF EXISTS `piwigo_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_search` (
                                 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                 `last_seen` date DEFAULT NULL,
                                 `rules` text DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_search`
--

LOCK TABLES `piwigo_search` WRITE;
/*!40000 ALTER TABLE `piwigo_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_sessions`
--

DROP TABLE IF EXISTS `piwigo_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_sessions` (
                                   `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                   `data` mediumtext NOT NULL,
                                   `expiration` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_sessions`
--

LOCK TABLES `piwigo_sessions` WRITE;
/*!40000 ALTER TABLE `piwigo_sessions` DISABLE KEYS */;
INSERT INTO `piwigo_sessions` VALUES ('7F004kaer8naccrflgiiigu2up64ne','pwg_device|s:7:\"desktop\";pwg_mobile_theme|b:0;pwg_caps|a:3:{i:0;s:1:\"1\";i:1;s:4:\"1920\";i:2;s:3:\"902\";}','2021-12-25 14:01:21'),('C0A8s38s54e7969l2k625a073q63jj','pwg_device|s:7:\"desktop\";pwg_mobile_theme|b:0;pwg_uid|i:1;pwg_tour_to_launch|s:19:\"tours/first_contact\";cache_activity_last_weeks|a:2:{s:13:\"calculated_on\";i:1612625760;s:4:\"data\";a:1:{i:3;a:1:{i:6;a:3:{s:7:\"details\";a:1:{s:4:\"User\";a:1:{s:5:\"Login\";s:1:\"1\";}}s:6:\"number\";i:1;s:4:\"date\";s:24:\"Vendredi 5 Février 2021\";}}}}pwg_caps|a:3:{i:0;s:1:\"1\";i:1;s:4:\"1920\";i:2;s:3:\"942\";}','2021-02-06 15:36:40');
/*!40000 ALTER TABLE `piwigo_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_sites`
--

DROP TABLE IF EXISTS `piwigo_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_sites` (
                                `id` tinyint(4) NOT NULL AUTO_INCREMENT,
                                `galleries_url` varchar(255) NOT NULL DEFAULT '',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `sites_ui1` (`galleries_url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_sites`
--

LOCK TABLES `piwigo_sites` WRITE;
/*!40000 ALTER TABLE `piwigo_sites` DISABLE KEYS */;
INSERT INTO `piwigo_sites` VALUES (1,'./galleries/');
/*!40000 ALTER TABLE `piwigo_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_tags`
--

DROP TABLE IF EXISTS `piwigo_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_tags` (
                               `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
                               `name` varchar(255) NOT NULL DEFAULT '',
                               `url_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                               `lastmodified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                               PRIMARY KEY (`id`),
                               KEY `tags_i1` (`url_name`),
                               KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_tags`
--

LOCK TABLES `piwigo_tags` WRITE;
/*!40000 ALTER TABLE `piwigo_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_themes`
--

DROP TABLE IF EXISTS `piwigo_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_themes` (
                                 `id` varchar(64) NOT NULL DEFAULT '',
                                 `version` varchar(64) NOT NULL DEFAULT '0',
                                 `name` varchar(64) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_themes`
--

LOCK TABLES `piwigo_themes` WRITE;
/*!40000 ALTER TABLE `piwigo_themes` DISABLE KEYS */;
INSERT INTO `piwigo_themes` VALUES ('modus','2.10.1','modus'),('smartpocket','2.10.1','Smart Pocket');
/*!40000 ALTER TABLE `piwigo_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_upgrade`
--

DROP TABLE IF EXISTS `piwigo_upgrade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_upgrade` (
                                  `id` varchar(20) NOT NULL DEFAULT '',
                                  `applied` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                  `description` varchar(255) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_upgrade`
--

LOCK TABLES `piwigo_upgrade` WRITE;
/*!40000 ALTER TABLE `piwigo_upgrade` DISABLE KEYS */;
INSERT INTO `piwigo_upgrade` VALUES ('100','2019-12-15 17:01:35','upgrade included in installation'),('101','2019-12-15 17:01:35','upgrade included in installation'),('102','2019-12-15 17:01:35','upgrade included in installation'),('103','2019-12-15 17:01:35','upgrade included in installation'),('104','2019-12-15 17:01:35','upgrade included in installation'),('105','2019-12-15 17:01:35','upgrade included in installation'),('106','2019-12-15 17:01:35','upgrade included in installation'),('107','2019-12-15 17:01:35','upgrade included in installation'),('108','2019-12-15 17:01:35','upgrade included in installation'),('109','2019-12-15 17:01:35','upgrade included in installation'),('110','2019-12-15 17:01:35','upgrade included in installation'),('111','2019-12-15 17:01:35','upgrade included in installation'),('112','2019-12-15 17:01:35','upgrade included in installation'),('113','2019-12-15 17:01:35','upgrade included in installation'),('114','2019-12-15 17:01:35','upgrade included in installation'),('115','2019-12-15 17:01:35','upgrade included in installation'),('116','2019-12-15 17:01:35','upgrade included in installation'),('117','2019-12-15 17:01:35','upgrade included in installation'),('118','2019-12-15 17:01:35','upgrade included in installation'),('119','2019-12-15 17:01:35','upgrade included in installation'),('120','2019-12-15 17:01:35','upgrade included in installation'),('121','2019-12-15 17:01:35','upgrade included in installation'),('122','2019-12-15 17:01:35','upgrade included in installation'),('123','2019-12-15 17:01:35','upgrade included in installation'),('124','2019-12-15 17:01:35','upgrade included in installation'),('125','2019-12-15 17:01:35','upgrade included in installation'),('126','2019-12-15 17:01:35','upgrade included in installation'),('127','2019-12-15 17:01:35','upgrade included in installation'),('128','2019-12-15 17:01:35','upgrade included in installation'),('129','2019-12-15 17:01:35','upgrade included in installation'),('130','2019-12-15 17:01:35','upgrade included in installation'),('131','2019-12-15 17:01:35','upgrade included in installation'),('132','2019-12-15 17:01:35','upgrade included in installation'),('133','2019-12-15 17:01:35','upgrade included in installation'),('134','2019-12-15 17:01:35','upgrade included in installation'),('135','2019-12-15 17:01:35','upgrade included in installation'),('136','2019-12-15 17:01:35','upgrade included in installation'),('137','2019-12-15 17:01:35','upgrade included in installation'),('138','2019-12-15 17:01:35','upgrade included in installation'),('139','2019-12-15 17:01:35','upgrade included in installation'),('140','2019-12-15 17:01:35','upgrade included in installation'),('141','2019-12-15 17:01:35','upgrade included in installation'),('142','2019-12-15 17:01:35','upgrade included in installation'),('143','2019-12-15 17:01:35','upgrade included in installation'),('144','2019-12-15 17:01:35','upgrade included in installation'),('145','2019-12-15 17:01:35','upgrade included in installation'),('146','2019-12-15 17:01:35','upgrade included in installation'),('147','2019-12-15 17:01:35','upgrade included in installation'),('148','2019-12-15 17:01:35','upgrade included in installation'),('149','2019-12-15 17:01:35','upgrade included in installation'),('150','2019-12-15 17:01:35','upgrade included in installation'),('151','2019-12-15 17:01:35','upgrade included in installation'),('152','2019-12-15 17:01:35','upgrade included in installation'),('153','2019-12-15 17:01:35','upgrade included in installation'),('154','2019-12-15 17:01:35','upgrade included in installation'),('155','2019-12-15 17:01:35','upgrade included in installation'),('156','2019-12-15 17:01:35','upgrade included in installation'),('157','2021-02-06 15:35:22','[migration from 2.10.0 to 11.3.0, 0.000 s] add config parameters to display smart app banner'),('158','2021-02-06 15:35:22','[migration from 2.10.0 to 11.3.0, 0.010 s] set default date to 1970-01-01'),('159','2021-02-06 15:35:22','[migration from 2.10.0 to 11.3.0, 0.003 s] add index on images.path'),('160','2021-12-25 14:00:56','[migration from 11.0.0 to 12.1.0, 0.002 s] add lounge table'),('161','2021-12-25 14:00:56','[migration from 11.0.0 to 12.1.0, 0.001 s] remove doubled activities on tag addition'),('162','2021-12-25 14:00:56','[migration from 11.0.0 to 12.1.0, 0.000 s] change activity.performed_by for logout'),('61','2019-12-15 17:01:35','upgrade included in installation'),('62','2019-12-15 17:01:35','upgrade included in installation'),('63','2019-12-15 17:01:35','upgrade included in installation'),('64','2019-12-15 17:01:35','upgrade included in installation'),('65','2019-12-15 17:01:35','upgrade included in installation'),('66','2019-12-15 17:01:35','upgrade included in installation'),('67','2019-12-15 17:01:35','upgrade included in installation'),('68','2019-12-15 17:01:35','upgrade included in installation'),('69','2019-12-15 17:01:35','upgrade included in installation'),('70','2019-12-15 17:01:35','upgrade included in installation'),('71','2019-12-15 17:01:35','upgrade included in installation'),('72','2019-12-15 17:01:35','upgrade included in installation'),('73','2019-12-15 17:01:35','upgrade included in installation'),('74','2019-12-15 17:01:35','upgrade included in installation'),('75','2019-12-15 17:01:35','upgrade included in installation'),('76','2019-12-15 17:01:35','upgrade included in installation'),('77','2019-12-15 17:01:35','upgrade included in installation'),('78','2019-12-15 17:01:35','upgrade included in installation'),('79','2019-12-15 17:01:35','upgrade included in installation'),('80','2019-12-15 17:01:35','upgrade included in installation'),('81','2019-12-15 17:01:35','upgrade included in installation'),('82','2019-12-15 17:01:35','upgrade included in installation'),('83','2019-12-15 17:01:35','upgrade included in installation'),('84','2019-12-15 17:01:35','upgrade included in installation'),('85','2019-12-15 17:01:35','upgrade included in installation'),('86','2019-12-15 17:01:35','upgrade included in installation'),('87','2019-12-15 17:01:35','upgrade included in installation'),('88','2019-12-15 17:01:35','upgrade included in installation'),('89','2019-12-15 17:01:35','upgrade included in installation'),('90','2019-12-15 17:01:35','upgrade included in installation'),('91','2019-12-15 17:01:35','upgrade included in installation'),('92','2019-12-15 17:01:35','upgrade included in installation'),('93','2019-12-15 17:01:35','upgrade included in installation'),('94','2019-12-15 17:01:35','upgrade included in installation'),('95','2019-12-15 17:01:35','upgrade included in installation'),('96','2019-12-15 17:01:35','upgrade included in installation'),('97','2019-12-15 17:01:35','upgrade included in installation'),('98','2019-12-15 17:01:35','upgrade included in installation'),('99','2019-12-15 17:01:35','upgrade included in installation');
/*!40000 ALTER TABLE `piwigo_upgrade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_access`
--

DROP TABLE IF EXISTS `piwigo_user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_access` (
                                      `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                      `cat_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                      PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_access`
--

LOCK TABLES `piwigo_user_access` WRITE;
/*!40000 ALTER TABLE `piwigo_user_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_auth_keys`
--

DROP TABLE IF EXISTS `piwigo_user_auth_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_auth_keys` (
                                         `auth_key_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                         `auth_key` varchar(255) NOT NULL,
                                         `user_id` mediumint(8) unsigned NOT NULL,
                                         `created_on` datetime NOT NULL,
                                         `duration` int(11) unsigned DEFAULT NULL,
                                         `expired_on` datetime NOT NULL,
                                         PRIMARY KEY (`auth_key_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_auth_keys`
--

LOCK TABLES `piwigo_user_auth_keys` WRITE;
/*!40000 ALTER TABLE `piwigo_user_auth_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_auth_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_cache`
--

DROP TABLE IF EXISTS `piwigo_user_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_cache` (
                                     `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                     `need_update` enum('true','false') NOT NULL DEFAULT 'true',
                                     `cache_update_time` int(10) unsigned NOT NULL DEFAULT 0,
                                     `forbidden_categories` mediumtext DEFAULT NULL,
                                     `nb_total_images` mediumint(8) unsigned DEFAULT NULL,
                                     `last_photo_date` datetime DEFAULT NULL,
                                     `nb_available_tags` int(5) DEFAULT NULL,
                                     `nb_available_comments` int(5) DEFAULT NULL,
                                     `image_access_type` enum('NOT IN','IN') NOT NULL DEFAULT 'NOT IN',
                                     `image_access_list` mediumtext DEFAULT NULL,
                                     PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_cache`
--

LOCK TABLES `piwigo_user_cache` WRITE;
/*!40000 ALTER TABLE `piwigo_user_cache` DISABLE KEYS */;
INSERT INTO `piwigo_user_cache` VALUES (2,'false',1640437270,'0',0,NULL,0,NULL,'NOT IN','0');
/*!40000 ALTER TABLE `piwigo_user_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_cache_categories`
--

DROP TABLE IF EXISTS `piwigo_user_cache_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_cache_categories` (
                                                `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                                `cat_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                                `date_last` datetime DEFAULT NULL,
                                                `max_date_last` datetime DEFAULT NULL,
                                                `nb_images` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                                `count_images` mediumint(8) unsigned DEFAULT 0,
                                                `nb_categories` mediumint(8) unsigned DEFAULT 0,
                                                `count_categories` mediumint(8) unsigned DEFAULT 0,
                                                `user_representative_picture_id` mediumint(8) unsigned DEFAULT NULL,
                                                PRIMARY KEY (`user_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_cache_categories`
--

LOCK TABLES `piwigo_user_cache_categories` WRITE;
/*!40000 ALTER TABLE `piwigo_user_cache_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_cache_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_feed`
--

DROP TABLE IF EXISTS `piwigo_user_feed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_feed` (
                                    `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                    `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                    `last_check` datetime DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_feed`
--

LOCK TABLES `piwigo_user_feed` WRITE;
/*!40000 ALTER TABLE `piwigo_user_feed` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_feed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_group`
--

DROP TABLE IF EXISTS `piwigo_user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_group` (
                                     `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                     `group_id` smallint(5) unsigned NOT NULL DEFAULT 0,
                                     PRIMARY KEY (`group_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_group`
--

LOCK TABLES `piwigo_user_group` WRITE;
/*!40000 ALTER TABLE `piwigo_user_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_infos`
--

DROP TABLE IF EXISTS `piwigo_user_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_infos` (
                                     `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                     `nb_image_page` smallint(3) unsigned NOT NULL DEFAULT 15,
                                     `status` enum('webmaster','admin','normal','generic','guest') NOT NULL DEFAULT 'guest',
                                     `language` varchar(50) NOT NULL DEFAULT 'en_UK',
                                     `expand` enum('true','false') NOT NULL DEFAULT 'false',
                                     `show_nb_comments` enum('true','false') NOT NULL DEFAULT 'false',
                                     `show_nb_hits` enum('true','false') NOT NULL DEFAULT 'false',
                                     `recent_period` tinyint(3) unsigned NOT NULL DEFAULT 7,
                                     `theme` varchar(255) NOT NULL DEFAULT 'modus',
                                     `registration_date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
                                     `enabled_high` enum('true','false') NOT NULL DEFAULT 'true',
                                     `level` tinyint(3) unsigned NOT NULL DEFAULT 0,
                                     `activation_key` varchar(255) DEFAULT NULL,
                                     `activation_key_expire` datetime DEFAULT NULL,
                                     `last_visit` datetime DEFAULT NULL,
                                     `last_visit_from_history` enum('true','false') NOT NULL DEFAULT 'false',
                                     `lastmodified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
                                     PRIMARY KEY (`user_id`),
                                     KEY `lastmodified` (`lastmodified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_infos`
--

LOCK TABLES `piwigo_user_infos` WRITE;
/*!40000 ALTER TABLE `piwigo_user_infos` DISABLE KEYS */;
INSERT INTO `piwigo_user_infos` VALUES (1,15,'webmaster','fr_FR','false','false','false',7,'modus','2019-12-15 17:01:35','true',8,NULL,NULL,'2021-02-06 15:36:40','false','2019-12-15 17:01:35'),(2,15,'guest','fr_FR','false','false','false',7,'modus','2019-12-15 17:01:35','true',0,NULL,NULL,'2021-12-25 14:01:10','false','2019-12-15 17:01:35');
/*!40000 ALTER TABLE `piwigo_user_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_user_mail_notification`
--

DROP TABLE IF EXISTS `piwigo_user_mail_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_user_mail_notification` (
                                                 `user_id` mediumint(8) unsigned NOT NULL DEFAULT 0,
                                                 `check_key` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                                 `enabled` enum('true','false') NOT NULL DEFAULT 'false',
                                                 `last_send` datetime DEFAULT NULL,
                                                 PRIMARY KEY (`user_id`),
                                                 UNIQUE KEY `user_mail_notification_ui1` (`check_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_user_mail_notification`
--

LOCK TABLES `piwigo_user_mail_notification` WRITE;
/*!40000 ALTER TABLE `piwigo_user_mail_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `piwigo_user_mail_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piwigo_users`
--

DROP TABLE IF EXISTS `piwigo_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piwigo_users` (
                                `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
                                `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
                                `password` varchar(255) DEFAULT NULL,
                                `mail_address` varchar(255) DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `users_ui1` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piwigo_users`
--

LOCK TABLES `piwigo_users` WRITE;
/*!40000 ALTER TABLE `piwigo_users` DISABLE KEYS */;
INSERT INTO `piwigo_users` VALUES (1,'piwigo_admin','$P$GDWswboU4J6ekp4XPAGDaslVvlCK6s0','cedric@bleschet.fr'),(2,'guest',NULL,NULL);
/*!40000 ALTER TABLE `piwigo_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'piwigo'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-25 14:05:32